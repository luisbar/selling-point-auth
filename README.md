## Howw to run?

1. Clone the repository with selling-point-db submodule
```bash
git clone --recursive  git@gitlab.com:luisbar/selling-point-api.git
```

2. Create `.env` file using the `.env.template` as reference

3. Install dependencies
```bash
npm i
```

4. Run the project
```bash
npm run start:dev
```

## Create public and provate key for the JWT

1. Create a private key
```bash
openssl ecparam -name secp256k1 -genkey -noout -out private.pem
```

Sample contents of the private.pem private key in PEM format:

-----BEGIN EC PRIVATE KEY-----
MHQCAQEEIEYgBlyQVsH7SpHUH7x4RErcckhu7ary/JjhP72Nk19EoAcGBSuBBAAK
oUQDQgAE1MtHIxlGP5TARqBccrddNm1FnYH1Fp+onETz5KbXPSeG5FGwKMUXGfAm
SZJq2gENULFewwymt+9bTXkjBZhh8A==
-----END EC PRIVATE KEY-----

2. Create a public key by extracting it from the private key.
```bash
openssl ec -in private.pem -pubout > public.pem
```

Sample contents of the public.pem public key in PEM format:

-----BEGIN PUBLIC KEY-----
MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAE1MtHIxlGP5TARqBccrddNm1FnYH1Fp+o
nETz5KbXPSeG5FGwKMUXGfAmSZJq2gENULFewwymt+9bTXkjBZhh8A==
-----END PUBLIC KEY-----