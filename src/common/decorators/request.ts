import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const Request = createParamDecorator((value: any, context: ExecutionContext) => {
  const request = context.switchToHttp().getRequest();
  return request;
})