import { PipeTransform, Injectable, ArgumentMetadata, HttpStatus, ValidationError } from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';
import { ApiException } from '../exception/api.exception';
import * as ramda from 'ramda';

@Injectable()
export class BodyValidationPipe implements PipeTransform<any> {

  getFields(errors: ValidationError[]) {
    return ramda.reduce(
      (accumulator, error) => {
        
        if (!ramda.isEmpty(ramda.pathOr({}, ['constraints'], error)))
          return ramda.assoc(error.property, ramda.compose(ramda.head, ramda.values)(error.constraints), accumulator);
        
        return ramda
        .reduce(
          (secondAccumulator, secondError) => {
            return ramda
            .reduce(
              (thirdAccumulator, thirdError) => {
                return ramda
                .assoc(
                  ramda
                  .join(
                    '.',
                    [
                      error.property,
                      secondError.property,
                      thirdError.property,
                    ],
                  ),
                  ramda.compose(
                    ramda.head,
                    ramda.values,
                  )(thirdError.constraints),
                  thirdAccumulator,
                );
              },
              secondAccumulator,
              secondError.children,
            )
          },
          accumulator,
          error.children
        )
      },
      {},
      errors
    )
  }

  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) return value;

    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    if (errors.length) throw new ApiException(
      'Bad Request',
      HttpStatus.BAD_REQUEST,
      'errors.body.invalid',
      this.getFields(errors),
    );

    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Boolean, Number, Array, Object];
    return !types.includes(metatype);
  }
}