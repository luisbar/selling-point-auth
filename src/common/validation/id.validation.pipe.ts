import { PipeTransform, Injectable, ArgumentMetadata, HttpStatus } from '@nestjs/common';
import { Request } from 'express';
import { I18nService } from 'nestjs-i18n';
import { ApiException } from '../exception/api.exception';

@Injectable()
export class IdValidationPipe implements PipeTransform {

  constructor(private readonly i18n: I18nService) {}

  async transform(request: Request, metadata: ArgumentMetadata) {
    const { id } = request.params;
    const lang = request.headers['accept-language'];

    if (isNaN(Number(id))) throw new ApiException(
      'Bad Request',
      HttpStatus.BAD_REQUEST,
      await this.i18n.translate(
        'user.id.invalid',
        {
          lang,
        }
      ),
      {}
    );

    return id;
  }
}
