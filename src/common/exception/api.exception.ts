import { HttpException } from '@nestjs/common';

export class ApiException extends HttpException {

  httpErrorMessage: string;
  fields: object;

  constructor(httpErrorMessage: string, httpStatus: number, message: string, fields: object) {
    super(message, httpStatus);
    this.httpErrorMessage = httpErrorMessage;
    this.fields = fields; 
  }
}