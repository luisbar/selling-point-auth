import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpStatus,
} from '@nestjs/common';
import * as ramda from 'ramda';
import { I18nService } from 'nestjs-i18n';
import { KeyValuePair } from 'ramda';
import { TraceException } from './trace.exception';

@Catch()
export class GenericExceptionFilter implements ExceptionFilter {

  constructor(private readonly i18n: I18nService) {}

  getHttpStatusCode(exception: Error) {
    return ramda.pathOr(HttpStatus.INTERNAL_SERVER_ERROR, ['status'], exception);
  }

  getHttpErrorMessage(exception: Error) {
    return ramda.pathOr('Internal server error', ['httpErrorMessage'], exception);
  }

  getMessage(exception: Error, lang: string) {
    return this.i18n.translate(ramda.pathOr(
      'errors.unknown',
      ['message'],
      exception
    ), {
      lang,
    });
  }

  async getFields(exception: Error, lang: string) {
    return ramda
    .fromPairs(
      ramda
      .splitEvery(
        2,
        await Promise.all(
          ramda
          .compose(
            ramda.once(ramda.flatten),
            ramda.map(([key, value]: string[]) => [key, this.i18n.translate(value, { lang, })],),
            ramda.toPairs,
            ramda.pathOr({}, ['fields']),
          )
          (exception)
        )
      ) as KeyValuePair<string, any>[]
    );
  }

  async catch(exception: Error, host: ArgumentsHost) {
    const context = host.switchToHttp();
    const response = context.getResponse();
    const request = context.getRequest();
    
    const httpStatusCode = this.getHttpStatusCode(exception);
    const httpErrorMessage = this.getHttpErrorMessage(exception);
    const message = await this.getMessage(exception, request.get('accept-language'));
    const fields = await this.getFields(exception, request.get('accept-language'));

    response
    .status(httpStatusCode)
    .json({
      httpStatusCode,
      httpErrorMessage,
      message,
      messages: exception.constructor.name === 'TraceException' ? (exception as TraceException).messages() : [],
      fields,
      path: request.url,
    });
  }
}