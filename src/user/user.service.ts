import { Injectable } from '@nestjs/common';
import { PrismaService } from '../common/prisma/prisma.service';
import { User, Prisma } from '@prisma/client';

@Injectable()
export class UserService {

  constructor(private prisma: PrismaService) {}

  findUnique(params: Prisma.UserFindUniqueArgs): Promise<User | null> {
    return this.prisma.user.findUnique(params);
  }

  findMany(params: Prisma.UserFindManyArgs): Promise<User[]> {
    return this.prisma.user.findMany(params);
  }

  create(params: Prisma.UserCreateArgs): Promise<User> {
    return this.prisma.user.create(params);
  }

  update(params: Prisma.UserUpdateArgs): Promise<User> {
    return this.prisma.user.update(params);
  }
}
