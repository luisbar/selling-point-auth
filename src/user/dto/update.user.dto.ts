import { IsString, IsNumber, MaxLength, Matches, IsEmail } from 'class-validator';

export class UpdateUserDto {

  @MaxLength(100, {
    message: 'errors.user.firstName.max.length',
  })
  @Matches(/^[a-zA-Z ñÑ\u00C0-\u017F]*$/, {
    message: 'errors.user.firstName.regex',
  })
  @IsString({
    message: 'errors.user.firstName.is.string'
  })
  firstName: string;

  @MaxLength(100, {
    message: 'errors.user.lastName.max.length',
  })
  @Matches(/^[a-zA-Z ñÑ\u00C0-\u017F]*$/, {
    message: 'errors.user.lastName.regex',
  })
  @IsString({
    message: 'errors.user.lastName.is.string'
  })
  lastName: string;

  @IsEmail({}, {
    message: 'errors.user.username.is.email',
  })
  @MaxLength(200, {
    message: 'errors.user.username.max.length',
  })
  @Matches(/^[a-zA-Z ñÑ\u00C0-\u017F]*$/, {
    message: 'errors.user.username.regex',
  })
  @IsString({
    message: 'errors.user.username.is.string'
  })
  username: string;

  @MaxLength(100, {
    message: 'errors.user.password.max.length',
  })
  @Matches(/^[a-zA-Z0-9]{3,30}$/, {
    message: 'errors.user.password.regex',
  })
  @IsString({
    message: 'errors.user.password.is.string'
  })
  password: string;

  @IsNumber({}, {
    message: 'errors.user.updatedby.is.number',
  })
  updatedById: number;

  @IsNumber({}, {
    message: 'errors.user.scope.is.number',
  })
  scopeId: number;
}


