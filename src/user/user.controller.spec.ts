import { Test, TestingModule } from '@nestjs/testing';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import * as path from 'path';
import { CommonModule } from '../common/common.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';

describe('UserController', () => {
  let controller: UserController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
      exports: [UserService],
      imports: [CommonModule, I18nModule.forRoot({
        fallbackLanguage: 'en',
        parser: I18nJsonParser,
        parserOptions: {
          path: path.join(__dirname, '../i18n/'),
        },
      })],
    }).compile();

    controller = module.get<UserController>(UserController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
