import { Body, Controller, Get, Post, Patch, Headers } from '@nestjs/common';
import { User, Prisma } from '@prisma/client';
import { BodyValidationPipe } from '../common/validation/body.validation.pipe';
import { CreateUserDto } from './dto/create.user.dto';
import { UpdateUserDto } from './dto/update.user.dto';
import { UserService } from './user.service';
import { LoggerService } from '../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';
import { IdValidationPipe } from '../common/validation/id.validation.pipe';
import { Request } from '../common/decorators/request';
import * as bcrypt from 'bcrypt';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
const PRISMA_ERROR_UNIQUE = 'P2002';
@ApiTags('user')
@ApiBearerAuth()
@Controller('auth/user')
export class UserController {
  
  constructor(
    private readonly userService: UserService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(UserController.name);
  }

  async getError(error: Error, errorMessage: string, lang: string): Promise<Error> {
    return this.logger.error(
      await this.i18n.translate(
        errorMessage,
        {
          lang,
        }
      ),
      error,
    );
  }

  @Post('/')
  async create(
    @Headers() headers,
    @Body(BodyValidationPipe) user: CreateUserDto
  ): Promise<User> {
    try {
      return await this.userService.create({
        data: {
          ...user,
          password: await bcrypt.hash(user.password, process.env.PASSWORD_SALT),
        }
      });
    } catch (error) {
      if (PRISMA_ERROR_UNIQUE === error.code)
        throw await this.getError(error, 'errors.user.username.repeated', headers['accept-language']);
      
      throw await this.getError(error, 'errors.user.cannot.create', headers['accept-language']);
    }
  }

  @Get('/')
  async findMany(
    @Headers() headers,
    @Body() params: Prisma.UserFindManyArgs
  ): Promise<User[]> {
    try {
      return await this.userService.findMany(params);
    } catch (error) {
      throw await this.getError(error, 'errors.user.cannot.find.many', headers['accept-language']);
    }
  }

  @ApiParam({
    name: 'id',
    type: 'string',
  })
  @Get('/:id')
  async findUnique(
    @Headers() headers,
    @Request(IdValidationPipe) id: number,
    @Body() params: Prisma.UserFindUniqueArgs
  ): Promise<User> {
    try {
      return await this.userService.findUnique({...params, where: { id: parseInt(String(id)), }});
    } catch (error) {
      throw await this.getError(error, 'errors.user.cannot.find.unique', headers['accept-language']);
    }
  }

  @ApiParam({
    name: 'id',
    type: 'string',
  })
  @Patch('/:id')
  async update(
    @Headers() headers,
    @Request(IdValidationPipe) id: number,
    @Body(BodyValidationPipe) data: UpdateUserDto
  ): Promise<User> {
    try {
      return await this.userService.update({ data, where: { id: parseInt(String(id)), }});
    } catch (error) {
      if (PRISMA_ERROR_UNIQUE === error.code)
        throw await this.getError(error, 'errors.user.username.repeated', headers['accept-language']);

      throw await this.getError(error, 'errors.user.cannot.update', headers['accept-language']);
    }
  }
}
