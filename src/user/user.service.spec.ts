import { Test, TestingModule } from '@nestjs/testing';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import { CommonModule } from '../common/common.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import * as path from 'path';

describe('UserService', () => {
  let service: UserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [UserService],
      exports: [UserService],
      imports: [CommonModule, I18nModule.forRoot({
        fallbackLanguage: 'en',
        parser: I18nJsonParser,
        parserOptions: {
          path: path.join(__dirname, '../i18n/'),
        },
      })],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
