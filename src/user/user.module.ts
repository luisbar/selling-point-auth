import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { CommonModule } from '../common/common.module';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import * as path from 'path';

@Module({
  providers: [UserService],
  exports: [UserService],
  imports: [CommonModule, I18nModule.forRoot({
    fallbackLanguage: 'en',
    parser: I18nJsonParser,
    parserOptions: {
      path: path.resolve(__dirname, '../i18n/'),
    },
  }),],
  controllers: [UserController],
})
export class UserModule {}
