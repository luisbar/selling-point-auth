import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from './common/logger/logger.service';
import { PrismaService } from './common/prisma/prisma.service';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as yaml from 'yaml';
import * as fs from 'fs';

const setupLogger = (app) => {
  app.useLogger(new LoggerService().setContext('Default'));
}

const setupPrisma = (app) => {
  const prismaService: PrismaService = app.get(PrismaService);
  prismaService.enableShutdownHooks(app);
}

const setupSwagger = (app) => {
  const config = new DocumentBuilder()
  .setTitle('Authentication server')
  .setDescription('Authentication server which authenticates and authorizes users')
  .setVersion('1.0.0')
  .addBearerAuth()
  .build();
  const document = SwaggerModule.createDocument(app, config);
  const yamlString: string = yaml.stringify(document, {});
  fs.writeFileSync('openapi.yaml', yamlString);
  SwaggerModule.setup('doc', app, document);
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });
  setupLogger(app);
  setupPrisma(app);
  setupSwagger(app);
  await app.listen(3000);
}
bootstrap();
