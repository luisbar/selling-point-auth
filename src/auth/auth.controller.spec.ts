import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import * as path from 'path';
import { CommonModule } from '../common/common.module';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './jwt.strategy';

describe('AuthController', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      imports: [
        UserModule,
        CommonModule,
        I18nModule.forRoot({
          fallbackLanguage: 'en',
          parser: I18nJsonParser,
          parserOptions: {
            path: path.resolve(__dirname, '../i18n/'),
          },
        }),
        JwtModule.register({
          secret: 'secret',
          signOptions: { expiresIn: '60s' },
        }),
        PassportModule,
      ],
      providers: [JwtStrategy]
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
