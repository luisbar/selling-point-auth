import { IsString, IsEmail, MaxLength, MinLength, Matches, IsOptional } from 'class-validator';

export class TokenDto {

  @MaxLength(100, {
    message: 'errors.token.username.max.length',
  })
  @IsEmail({}, {
    message: 'errors.token.username.is.email',
  })
  @IsString({
    message: 'errors.token.username.is.string',
  })
  username: string;

  @MinLength(5, {
    message: 'errors.token.password.min.length',
  })
  @MaxLength(100, {
    message: 'errors.token.password.max.length',
  })
  @Matches(/^[a-zA-Z0-9]{3,30}$/, {
    message: 'errors.token.password.regex',
  })
  @IsString({
    message: 'errors.token.password.is.string',
  })
  password: string;

  @Matches(/^(password|refresh_token)$/, {
    message: 'errors.token.grantType.regex',
  })
  @IsString({
    message: 'errors.token.grantType.is.string',
  })
  grantType: string;
  
  @IsOptional()
  @IsString({
    message: 'errors.token.refreshToken.is.string',
  })
  refreshToken: string;
}