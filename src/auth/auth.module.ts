import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import * as path from 'path';
import { CommonModule } from '../common/common.module';
import { UserModule } from '../user/user.module';
import { AuthController } from './auth.controller'
import { JwtStrategy } from './jwt.strategy';
import * as fs from 'fs'


@Module({
  controllers: [AuthController],
  imports: [
    UserModule,
    CommonModule,
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      parser: I18nJsonParser,
      parserOptions: {
        path: path.resolve(__dirname, '../i18n/'),
      },
    }),
    JwtModule.register({
      privateKey: fs.readFileSync(path.resolve(__dirname, '../private.pem')),
      publicKey: fs.readFileSync(path.resolve(__dirname, '../public.pem')),
      signOptions: {
        expiresIn: '12h',
        algorithm: 'RS256',
      },
    }),
    PassportModule,
  ],
  providers: [JwtStrategy]
})
export class AuthModule {}
