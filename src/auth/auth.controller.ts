import { Body, Controller, Get, Headers, HttpStatus, Post } from '@nestjs/common';
import { UserService } from '../user/user.service'
import { JwtService } from '@nestjs/jwt';
import { LoggerService } from '../common/logger/logger.service';
import { I18nService } from 'nestjs-i18n';
import { BodyValidationPipe } from '../common/validation/body.validation.pipe';
import { Public } from '../common/decorators/public';
import * as bcrypt from 'bcrypt';
import { ApiException } from '../common/exception/api.exception';
import { TokenDto } from './dto/token.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('auth')
@ApiBearerAuth()
@Controller('auth')
export class AuthController {
  
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService,
  ) {}

  async getError(error: Error, errorMessage: string, lang: string): Promise<Error> {
    return this.logger.error(
      await this.i18n.translate(
        errorMessage,
        {
          lang,
        }
      ),
      error,
    );
  }

  @Public()
  @Post('/token')
  async token(
    @Headers() headers,
    @Body(BodyValidationPipe) body: TokenDto,
  ): Promise<object> {
    try {
      const user = await this.userService.findUnique({
        where: {
          username: body.username,
        },
        include: {
          scope: true,
        },
      });
      
      if (!user || !await bcrypt.compare(body.password, user.password))
        throw new ApiException(
          'Bad Request',
          HttpStatus.BAD_REQUEST,
          await this.i18n.translate(
            'errors.auth.credentials.invalid',
            {
              lang: headers['accept-language'],
            },
          ),
          {},
        );
      
      return {
        accessToken: this.jwtService.sign({
          userId: user.id,
          firstName: user.firstName,
          lastName: user.lastName,
          username: user.username,
          scope: user['scope']['rules'],
        }),
      };
    } catch (error) {
      throw await this.getError(error, 'errors.auth.cannot.login', headers['accept-language']);
    }
  }

  @Get('/authorize')
  async authorize(
    @Headers() headers,
  ): Promise<object> {
    try {
      const token = String(headers['authorization']).replace('Bearer ', '');
      return this.jwtService.verify(token);
    } catch (error) {
      throw await this.getError(error, 'errors.auth.cannot.authorize', headers['accept-language']);
    }
  }
}
