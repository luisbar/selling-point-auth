import { ExecutionContext, HttpStatus, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AuthGuard } from '@nestjs/passport';
import { IS_PUBLIC_KEY } from '../common/decorators/public';
import { ApiException } from '../common/exception/api.exception';

@Injectable()
export class JwtAuthGuard extends AuthGuard('jwt') {

  constructor(private reflector: Reflector) {
    super();
  }

  canActivate(context: ExecutionContext) {
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic)
      return true;

    return super.canActivate(context);
  }

  handleRequest(error, user, info) {
    if (error || !user)
      throw new ApiException(
        'Forbidden',
        HttpStatus.FORBIDDEN,
        'errors.forbidden',
        {},
      );
    
    return user;
  }
}
