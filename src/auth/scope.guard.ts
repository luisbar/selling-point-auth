import { ExecutionContext, HttpStatus, Injectable, CanActivate } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { IS_PUBLIC_KEY } from '../common/decorators/public';
import { ApiException } from '../common/exception/api.exception';
import { Ability } from '@casl/ability';
import * as ramda from 'ramda';

@Injectable()
export class ScopeGuard implements CanActivate {

  constructor(
    private reflector: Reflector,
    private jwtService: JwtService
  ) {
  }

  capitalize() {
    return ramda.compose(
      ramda.join(''),
      ramda.juxt([
        ramda.compose(ramda.toUpper, ramda.head),
        ramda.tail
      ]),
      ramda.head,
      ramda.slice(1, 2),
      ramda.split('/'),
    );
  }

  canActivate(context: ExecutionContext) {
    const methodToAction = {
      post: 'create',
      put: 'update',
      patch: 'update',
      get: 'read',
    };
    const isPublic = this.reflector.getAllAndOverride<boolean>(IS_PUBLIC_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic)
      return true;

    const request = context.switchToHttp().getRequest();
    const encodedToken = request.headers['authorization'].replace('Bearer', '').trim();
    const decodedToken = this.jwtService.decode(encodedToken);
    const ability = new Ability(decodedToken['scope']);
    const action = methodToAction[request.method.toLowerCase()];
    const subject = ramda.ifElse(ramda.isNil, ramda.identity, this.capitalize())(request.url);
    
    if (ability.can(action, subject)) return true;

    throw new ApiException(
      'Forbidden',
      HttpStatus.FORBIDDEN,
      'errors.forbidden',
      {},
    );
  }
}
