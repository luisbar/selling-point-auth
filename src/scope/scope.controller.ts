import { Body, Controller, Get, Headers, Patch, Post } from '@nestjs/common';
import { I18nService } from 'nestjs-i18n';
import { LoggerService } from '../common/logger/logger.service';
import { BodyValidationPipe } from '../common/validation/body.validation.pipe';
import { CreateScopeDto } from './dto/create.scope.dto';
import { ScopeService } from './scope.service';
import { Scope, Prisma } from '@prisma/client';
import { Request } from '../common/decorators/request';
import { IdValidationPipe } from '../common/validation/id.validation.pipe';
import { UpdateScopeDto } from './dto/update.scope.dto';
import { ApiBearerAuth, ApiParam, ApiTags } from '@nestjs/swagger';
const PRISMA_ERROR_UNIQUE = 'P2002';

@ApiTags('scope')
@ApiBearerAuth()
@Controller('auth/scope')
export class ScopeController {

  constructor(
    private readonly scopeService: ScopeService,
    private readonly logger: LoggerService,
    private readonly i18n: I18nService
  ) {
    this.logger.setContext(ScopeController.name);
  }

  async getError(error: Error, errorMessage: string, lang: string): Promise<Error> {
    return this.logger.error(
      await this.i18n.translate(
        errorMessage,
        {
          lang,
        }
      ),
      error,
    );
  }

  @Post('/')
  async create(
    @Headers() headers,
    @Body(BodyValidationPipe) scope: CreateScopeDto
  ): Promise<Scope> {
    try {
      const rules = scope.rules as unknown;
      return await this.scopeService.create({ data: { ...scope, rules } } as Prisma.ScopeCreateArgs );
    } catch (error) {
      if (PRISMA_ERROR_UNIQUE === error.code)
        throw await this.getError(error, 'errors.scope.name.repeated', headers['accept-language']);

      throw await this.getError(error, 'errors.scope.cannot.create',  headers['accept-language']);
    }
  }

  @Get('/')
  async findMany(
    @Headers() headers,
    @Body() params: Prisma.ScopeFindManyArgs
  ): Promise<Scope[]> {
    try {
      return await this.scopeService.findMany(params);
    } catch (error) {
      throw await this.getError(error, 'errors.scope.cannot.find.many',  headers['accept-language']);
    }
  }

  @ApiParam({
    name: 'id',
    type: 'string',
  })
  @Get('/:id')
  async findUnique(
    @Headers() headers,
    @Request(IdValidationPipe) id: number,
    @Body() params: Prisma.ScopeFindUniqueArgs
  ): Promise<Scope> {
    try {
      return await this.scopeService.findUnique({...params, where: { id: parseInt(String(id)), }});
    } catch (error) {
      throw await this.getError(error, 'errors.scope.cannot.find.unique',  headers['accept-language']);
    }
  }

  @ApiParam({
    name: 'id',
    type: 'string',
  })
  @Patch('/:id')
  async update(
    @Headers() headers,
    @Request(IdValidationPipe) id: number,
    @Body(BodyValidationPipe) scope: UpdateScopeDto
  ): Promise<Scope> {
    try {
      const rules = scope.rules as unknown;
      return await this.scopeService.update({ data: { ...scope, rules }, where: { id: parseInt(String(id)), }} as Prisma.ScopeUpdateArgs);
    } catch (error) {
      if (PRISMA_ERROR_UNIQUE === error.code)
        throw await this.getError(error, 'errors.scope.name.repeated', headers['accept-language']);
      
      throw await this.getError(error, 'errors.scope.cannot.update',  headers['accept-language']);
    }
  }
}
