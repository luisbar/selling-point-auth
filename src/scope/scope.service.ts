import { Scope, Prisma } from '@prisma/client';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../common/prisma/prisma.service';

@Injectable()
export class ScopeService {

  constructor(private prisma: PrismaService) {}

  findUnique(params: Prisma.ScopeFindUniqueArgs): Promise<Scope> {
    return this.prisma.scope.findUnique(params);
  }

  findMany(params: Prisma.ScopeFindManyArgs): Promise<Scope[]> {
    return this.prisma.scope.findMany(params);
  }

  create(params: Prisma.ScopeCreateArgs): Promise<Scope> {
    return this.prisma.scope.create(params);
  }

  update(params: Prisma.ScopeUpdateArgs): Promise<Scope> {
    return this.prisma.scope.update(params);
  }
}
