import { Test, TestingModule } from '@nestjs/testing';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';
import * as path from 'path';
import { CommonModule } from '../common/common.module';
import { ScopeController } from './scope.controller';
import { ScopeService } from './scope.service';

describe('ScopeController', () => {
  let controller: ScopeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ScopeService],
      controllers: [ScopeController],
      imports: [CommonModule, I18nModule.forRoot({
        fallbackLanguage: 'en',
        parser: I18nJsonParser,
        parserOptions: {
          path: path.resolve(__dirname, '../i18n/'),
        },
      }),],
    }).compile();

    controller = module.get<ScopeController>(ScopeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
