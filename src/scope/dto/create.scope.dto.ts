import { IsString, IsNumber, MaxLength, Matches, IsArray, ValidateNested, ArrayMinSize, IsBoolean, IsOptional } from 'class-validator';
import { Type } from 'class-transformer';

export class CreateScopeDto {

  @MaxLength(50, {
    message: 'errors.scope.name.max.length',
  })
  @Matches(/^[a-zA-Z ñÑ\u00C0-\u017F]*$/, {
    message: 'errors.scope.name.regex',
  })
  @IsString({
    message: 'errors.scope.name.is.string',
  })
  name: string;

  @ValidateNested({
    each: true,
    message: 'errors.scope.rules.nested',
  })
  @Type(() => Rule)
  @ArrayMinSize(1, {
    message: 'errors.scope.rules.min.size',
  })
  @IsArray({
    message: 'errors.scope.rules.is.array',
  })
  rules: Rule[];

  @IsNumber({}, {
    message: 'errors.scope.createdby.is.number',
  })
  createdById: number;
}

class Rule {

  @ArrayMinSize(1, {
    message: 'errors.scope.rules.action.min.size',
  })
  @IsArray({
    message: 'errors.scope.rules.action.is.array',
  })
  action: string[];

  @ArrayMinSize(1, {
    message: 'errors.scope.rules.subject.min.size',
  })
  @IsArray({
    message: 'errors.scope.rules.subject.is.array',
  })
  subject: string[];

  @IsOptional()
  @IsArray({
    message: 'errors.scope.rules.fields.is.array',
  })
  fields: string[];

  coditions: any;
  
  @IsOptional()
  @IsBoolean({
    message: 'errors.scope.rules.inverted.is.boolean',
  })
  inverted: boolean;
  
  @IsOptional()
  @IsString({
    message: 'errors.scope.rules.reason.is.string',
  })
  reason: string;
}