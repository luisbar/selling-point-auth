import { Module } from '@nestjs/common';
import { ScopeService } from './scope.service';
import { ScopeController } from './scope.controller';
import { CommonModule } from '../common/common.module';
import * as path from 'path';
import { I18nJsonParser, I18nModule } from 'nestjs-i18n';

@Module({
  providers: [ScopeService],
  controllers: [ScopeController],
  imports: [CommonModule, I18nModule.forRoot({
    fallbackLanguage: 'en',
    parser: I18nJsonParser,
    parserOptions: {
      path: path.resolve(__dirname, '../i18n/'),
    },
  }),],
})
export class ScopeModule {}
