import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';
import { CommonModule } from './common/common.module';
import { I18nModule, I18nJsonParser } from 'nestjs-i18n';
import * as path from 'path';
import { APP_FILTER, APP_GUARD } from '@nestjs/core';
import { GenericExceptionFilter } from './common/exception/generic.exception.filter';
import { ScopeModule } from './scope/scope.module';
import { JwtAuthGuard } from './auth/jwt.guard';
import { ConfigModule } from '@nestjs/config';
import * as joi from 'joi';
import { ScopeGuard } from './auth/scope.guard';
import { JwtModule } from '@nestjs/jwt';
import * as fs from 'fs';

@Module({
  imports: [
    AuthModule,
    UserModule,
    CommonModule,
    I18nModule.forRoot({
      fallbackLanguage: 'en',
      parser: I18nJsonParser,
      parserOptions: {
        path: path.join(__dirname, '/i18n/'),
        watch: true,
      },
    }),
    ScopeModule,
    ConfigModule.forRoot({
      ignoreEnvFile: Boolean(process.env.IGNORE_ENV_FILE),
      isGlobal: true,
      validationSchema: joi.object({
        DATABASE_URL: joi.string().required(),
        PASSWORD_SALT: joi.string().required(),
        NODE_ENV: joi.string().required(),
      }),
    }),
    JwtModule.register({
      privateKey: fs.readFileSync(path.resolve(__dirname, './private.pem')),
      publicKey: fs.readFileSync(path.resolve(__dirname, './public.pem')),
      signOptions: {
        expiresIn: '12h',
        algorithm: 'RS256',
      },
    }),
  ],
  providers: [
    {
      provide: APP_FILTER,
      useClass: GenericExceptionFilter,
    },
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: ScopeGuard,
    },
  ],
})

// TODO: add helmet
// TODO: add rate limiting
// TODO: add cors
// TODO: add csfr protection
// TODO: add caching
// TODO: add token whitelist
// TODO: add refresh token
// TODO: add tests
// TODO: add constraint for accesing to other resources (an user must access its own data)

export class AppModule {}
