FROM node:16.5.0-alpine
WORKDIR /app
EXPOSE 3000
COPY . .
RUN npm i
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.0/wait /wait
RUN chmod +x /wait
CMD /wait && cd selling-point-db && npm i && npx prisma migrate dev && npx prisma db seed && cd .. && npm run start:dev